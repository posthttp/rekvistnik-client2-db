module database-rekvistnik

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/jinzhu/gorm v1.9.10
	github.com/mattn/go-sqlite3 v1.11.0
)
