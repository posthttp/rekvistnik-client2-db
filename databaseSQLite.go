package database-rekvistnik

import (
	"fmt"
	"os"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/jinzhu/gorm"

	// драйвер подключения к бае данных
	_ "github.com/mattn/go-sqlite3"
)

// SQLite - объект базы данных
type SQLite struct {
	DB *gorm.DB
}

//----------------------------------------------------------------------------------------------------
// TABLES
//----------------------------------------------------------------------------------------------------

// Videos - таблица базы данных
type Videos struct {
	gorm.Model
	IDItem int

	// Video
	IDVideo    int
	Name       string
	FileURL    string
	ClientID   int
	Duration   float32
	ShowCount  int
	SHA256     string
	IsArchive  bool
	CompanyID  int
	Counter    int
	Position   int
	StartDate  time.Time
	FinishDate time.Time

	// Time of video
	IDTime        int
	Title         string
	Weeks         string
	CompanyIDTime int
	CreatedAtTime time.Time
	UpdatedAtTime time.Time

	// Mode режим воспроизведения
	// none   (Не воспроизводить)
	// normal (Нормальный режим)
	// x2     (X2 раз воспроизведение)
	// x4     (X4 раз воспроизведение)
	Mode string

	// FilePath - путь к файлу на диске
	FileName string
	// State - состояние записи, варианты: [ create | update | download | ready ]
	// nil => create      (создана новая запись, файл не скачан)
	// create => update   (запись была обновлена, файл не актуален)
	// update => download (файл загружается)
	// download => ready  (файл загружен, запись актуальна)
	State string
	// Actual - флаг проверки неактуальных записей, которых нет в Playlist
	Actual bool
	// IsDelete - флаг удаления записи
	IsDelete bool
}

// MCHS - таблица MCHS оповещений
type MCHS struct {
	gorm.Model
	IDMessage int
	Title     string
	Body      string
	Start     time.Time
	Finish    time.Time
	Status    string
	Prioritet int
	// IsDelete - флаг удаления записи
	IsDelete bool
}

// Services - таблица сервисов
type Services struct {
	gorm.Model
	Name        string // название сервиса
	Status      string // статус сервиса [ work | wait | stop ]
	WaitService string // название сервиса, который ожидает
}

// SystemInfo - таблица системной информации
type SystemInfo struct {
	gorm.Model
	Type  string // Тип системной информации
	Value string // Значение типа информации
}

// Activation - таблица данных активации
type Activation struct {
	gorm.Model
	// Статус активации
	// 		not_created - точка не создана
	// 		new			- точка создана, не активирована
	// 		active		- точка активирована
	// 		no_active	- ---
	// 		exp_lic		- срок лицензии истек
	Status      string
	ActivateKey string // Ключ активации для новой точки
}

// LogPlay - таблица логов воспроизведения видео-роликов
type LogPlay struct {
	gorm.Model
	VideoID int       `json:"video_id"` // ID видео ролика
	At      time.Time `json:"at"`       // Дата воспроизведения
}

// LogMCHS - таблица логов МЧС сообщений
type LogMCHS struct {
	gorm.Model
	MCHSMessageID int       `json:"mchs_message_id"` // ID МЧС сообщения
	At            time.Time `json:"at"`              // Дата воспроизведения
}

//----------------------------------------------------------------------------------------------------
// SETTINGS
//----------------------------------------------------------------------------------------------------

// tomlConfig - настройки программы
type tomlConfig struct {
	Title   string
	Version string
	DSN     dsnInfo `toml:"database"`
}
type dsnInfo struct {
	Sudb     string
	DBPath   string
	User     string
	Password string
}

//----------------------------------------------------------------------------------------------------
// METHODS
//----------------------------------------------------------------------------------------------------

func readToml(fileName string) (tomlConfig, error) {
	var tc tomlConfig
	if _, err := toml.DecodeFile(fileName, &tc); err != nil {
		return tc, err
	}
	return tc, nil
}

// ConnectDataBase - подключение к базе данных
func (s *SQLite) ConnectDataBase() error {
	fileName := "./config_db.toml"
	if _, err := os.Stat(fileName); err != nil {
		fmt.Println("File:", fileName, "not found ERROR")
		return err
	}

	tc, err := readToml(fileName)
	if err != nil {
		fmt.Println("TOML read:", err)
		return err
	}
	fmt.Println("Open:", tc.Title, tc.Version)

	connStr := "file:" + tc.DSN.DBPath + "?" +
		"&_auth_user=" + tc.DSN.User +
		"&_auth_pass=" + tc.DSN.Password

	db, err := gorm.Open(tc.DSN.Sudb, connStr)
	if err != nil {
		fmt.Println("DataBase:", tc.DSN.Sudb, "open ERROR -", connStr, err)
		return err
	}
	fmt.Println("DataBase: open -", connStr)

	db.DB()
	err = db.DB().Ping() // первое подключение к базе
	if err != nil {
		fmt.Println("DataBase:", tc.DSN.Sudb, "connect ERROR")
		return err
	}

	// Migration
	db.AutoMigrate(&Videos{}, &MCHS{}, &Services{}, &SystemInfo{}, &Activation{}, &LogPlay{}, &LogMCHS{})

	s.DB = db
	fmt.Println("DataBase: connect success")
	return nil
}
